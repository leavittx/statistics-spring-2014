#include "TrueTrajectory.h"

#define _DEBUG 0

Trajectory TrueTrajectory::generateTrueTrajectory(const TrajectoryParams& params)
{
  Trajectory tr;

  // Select random initial position
  float x = randomUniform(), y = randomUniform();
  tr.initialPosition = Point(x, y);

  // Initial image
  Image im;
  im.emplace(tr.initialPosition);

#if _DEBUG
  printf("Robot position: (%5.3f, %5.3f)\n", x, y);
#endif // _DEBUG

  // Append additive noise points
  uint numNoisePointsEmplaced = 0;
  for (; numNoisePointsEmplaced < params.numberOfNoisePoints; )
  {
    float dx = randomUniform(-params.varianceAdditiveX, params.varianceAdditiveX);
    float dy = randomUniform(-params.varianceAdditiveY, params.varianceAdditiveY);
    if (im.emplace(Point(x + dx, y + dy)))
    {
      numNoisePointsEmplaced++;
    }
  }
  tr.trajectory.push_back(im);
  
  float motionNoisePreviousX = 0, motionNoisePreviousY = 0;
  for (uint t = 1; t < params.T; ++t)
  {
    Image im;

    // Compute noise
    float motionNoiseX = randomUniform(-params.varianceMotionX, params.varianceMotionX);
    float motionNoiseY = randomUniform(-params.varianceMotionY, params.varianceMotionY);
    
    // Simulate model trajectory (with autoregression)
    x = x + params.a - params.b*x - motionNoisePreviousX + motionNoiseX;
    y = y + params.c - params.d*y - motionNoisePreviousY + motionNoiseY;

    // Append new robot position
    im.emplace(Point(x, y));

#if _DEBUG
    printf("Robot position: (%5.3f, %5.3f)\n", x, y);
#endif // _DEBUG

    // Save noise for next step
    motionNoisePreviousX = motionNoiseX;
    motionNoisePreviousY = motionNoiseY;

    // Append additive noise points
    numNoisePointsEmplaced = 0;
    for (; numNoisePointsEmplaced < params.numberOfNoisePoints; )
    {
      float dx = randomUniform(-params.varianceAdditiveX, params.varianceAdditiveX);
      float dy = randomUniform(-params.varianceAdditiveY, params.varianceAdditiveY);
      if (im.emplace(Point(x + dx, y + dy)))
      {
        numNoisePointsEmplaced++;
      }
    }

    // Append new image to trajectory
    tr.trajectory.push_back(im);
  }

  // Save params used for trajectory generation
  tr.params = params;

  return tr;
}
