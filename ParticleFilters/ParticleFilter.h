#pragma once

#include <unordered_map>

#include "TrueTrajectory.h"

struct ParticleFilterParams
{
  float aMin, aMax;
  float bMin, bMax;
  float cMin, cMax;
  float dMin, dMax;

  uint nParticles;
};

struct Particle : Trajectory
{
  Particle() : currentPosition(), importanceWeight(0), importanceWeightsSum(0)/*, importanceWeights()*/ { }

  //! Current robot position
  Point currentPosition;
  //! Particle importance weight
  float importanceWeight;
  float importanceWeightsSum;
  //std::vector<float> importanceWeights;

  void printInfo() const
  {
    //printf("Importance weight: %.3f; Model params: (a=%5.2f, c=%5.2f)\n", importanceWeight, params.a, params.c);
    printf("Importance weight: %.3f; Model params: (a=%5.2f, b=%5.2f, c=%5.2f, d=%5.2f)\n",
           importanceWeight, params.a, params.b, params.c, params.d);
  }
};

class ParticleFilter
{
public:
  ParticleFilter();
  virtual ~ParticleFilter();

  void setTrueTrajectory(const Trajectory& trajectory);
  void process(const ParticleFilterParams& params);
  void printResults(int maxParticles = 10, bool printClosestToTrue = true);
  void printErrorForBestParticle();

private:
  float calculateDenormalizedImportanceWeight(const std::vector<Point>& pts, const Point& position);

private:
  Trajectory mTrueTrajectory;
  bool       mTrueTrajectorySet;
  bool       mFinished;
  std::vector<Particle> mParticles;
  std::unordered_map<uint, std::vector<Point> > mStepToImageMap;
};


