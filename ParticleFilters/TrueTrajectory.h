#pragma once

#include <vector>

#include "Common.h"
#include "Image.h"

//! Trajectory parameters
struct TrajectoryParams
{
  //! Number of time steps in trajectory
  uint  T;
  //! Model params
  float a, b, c, d;
  //! Noise params
  //! Number of non-robot additive noise points
  uint  numberOfNoisePoints;
  //! Additive noise (non-robot points) X/Y axis variance
  float varianceAdditiveX, varianceAdditiveY;
  //! Robot motion X/Y axis variance
  float varianceMotionX, varianceMotionY;
};

struct Trajectory
{
  //! Params used for trajectory generation
  TrajectoryParams   params;
  //! Trajectory of images
  std::vector<Image> trajectory;
  //! Initial robot position
  Point              initialPosition;
};

class TrueTrajectory
{
public:
  static Trajectory generateTrueTrajectory(const TrajectoryParams& params);
};