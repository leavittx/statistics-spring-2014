#include <iostream>

#include "Common.h"
#include "ParticleFilter.h"
#include "TrueTrajectory.h"

using namespace std;

int main()
{
  // Initialize pseudo-random number generator 
  randomInitialize();

  cout << ">>> Generating true trajectory" << endl;


  const uint valuesN[] = {/*1000, 10000, 100000, */1000000/*, 2000000, 2000000, 5000000*/};
#define NELEMS(a) (sizeof(a) / sizeof(*(a)))

  //for (uint T = 1; T <= 512; T *= 2)
  //uint T = 500;
  uint T = 100;
  {
    for (uint i = 0; i < NELEMS(valuesN); ++i)
    //uint N = 1000000;
    {
      uint N = valuesN[i];

      TrajectoryParams tParams;
      tParams.T = T;
#if 1
      tParams.a = 0.3f;
      tParams.b = 0.1f;
      tParams.c = 0.7f;
      tParams.d = 0.1f;
      tParams.numberOfNoisePoints = 49;
      tParams.varianceAdditiveX = 0.01f;
      tParams.varianceAdditiveY = 0.01f;
      tParams.varianceMotionX = 0.05f;
      tParams.varianceMotionY = 0.05f;
#else
      tParams.a = 0.12f;
      tParams.b = 0.039f;
      tParams.c = 0.98f;
      tParams.d = 0.069f;
      tParams.numberOfNoisePoints = 49;
      tParams.varianceAdditiveX = 0.01f;
      tParams.varianceAdditiveY = 0.01f;
      tParams.varianceMotionX = 0.05f;
      tParams.varianceMotionY = 0.05f;
#endif

      Trajectory trueTrajectory = TrueTrajectory::generateTrueTrajectory(tParams);

      //cout << ">>> Starting particle filter" << endl;

      ParticleFilterParams pfParams;
      pfParams.aMin = pfParams.cMin =  0.f;
      pfParams.aMax = pfParams.cMax =  1.f;
      pfParams.bMin = pfParams.dMin =  0.f;
      pfParams.bMax = pfParams.dMax =  0.1f;
      pfParams.nParticles = N;

      ParticleFilter pf;
      pf.setTrueTrajectory(trueTrajectory);
      pf.process(pfParams);
      pf.printResults(100, true);
      //pf.printErrorForBestParticle();
    }
  }
}