#pragma once

#include <cstdlib>
#include <ctime>
#include <cassert>

typedef unsigned int uint;

//! Initialize pseudo-random number generator
inline void randomInitialize()
{
  srand(uint(time(0)));
}

inline float randomUniform(float min = 0, float max = RAND_MAX)
{
  assert(min < max);
  return min + (max - min) * rand() / RAND_MAX;
}