#pragma once

#include <set>

//! Point description
struct Point
{
  Point(float x_ = 0.f, float y_ = 0.f)
    : x(x_), y(y_) { }

  float euclidianDistanceTo(const Point& other) const
  {
    return sqrt(euclidianDistance2To(other));
  }

  float euclidianDistance2To(const Point& other) const
  {
    float dx = x - other.x;
    float dy = y - other.y;

    return dx*dx + dy*dy;
  }

  float x, y;
};

inline bool operator < (const Point& p1, const Point& p2)
{
  if (p1.x != p2.x)
    return p1.x < p2.x;

  return p1.y < p2.y;
}

//! Image description
class Image
{
public:
  Image() : mPoints() { }
  virtual ~Image() { }

public:
  bool emplace(const Point& p)
  {
    // Notify user about duplicate
    if (contains(p))
    {
      return false;
    }
    mPoints.emplace(p);
    return true;
  }
  bool contains(const Point& p) const
  {
    auto it = mPoints.find(p);
    return (it != mPoints.end());
  }
  std::vector<Point> toVector() const
  {
    std::vector<Point> pts;
    for (auto it = mPoints.begin(); it != mPoints.end(); ++it)
    {
      pts.push_back(*it);
    }
    return pts;
  }

private:
  std::set<Point> mPoints;
};

  