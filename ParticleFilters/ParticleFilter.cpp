#include "ParticleFilter.h"

#include <iostream>
#include <algorithm>

#pragma warning(disable:4996)

ParticleFilter::ParticleFilter()
  : mTrueTrajectory(),
    mTrueTrajectorySet(false),
    mFinished(false),
    mParticles(),
    mStepToImageMap()
{

}

ParticleFilter::~ParticleFilter()
{

}

void ParticleFilter::setTrueTrajectory(const Trajectory& trajectory)
{
  mTrueTrajectory = trajectory;

  // Convert trajectory images to std::vector's of points
  for (uint t = 0; t < mTrueTrajectory.trajectory.size(); ++t)
  {
    mStepToImageMap[t] = mTrueTrajectory.trajectory[t].toVector();
  }

  mTrueTrajectorySet = true;
  mFinished = false;
}

void ParticleFilter::process(const ParticleFilterParams& params)
{
  // Select random model parameters for each model trajectory
  for (uint tr = 0; tr < params.nParticles; ++tr)
  {
    TrajectoryParams tParams;

    tParams.a = randomUniform(params.aMin, params.aMax);
    tParams.b = randomUniform(params.bMin, params.bMax);
    tParams.c = randomUniform(params.cMin, params.cMax);
    tParams.d = randomUniform(params.dMin, params.dMax);

    Particle p;
    p.params = tParams;
    p.currentPosition = mTrueTrajectory.initialPosition;

    mParticles.push_back(p);
  }


  // For each step
  for (uint t = 0; t < mTrueTrajectory.trajectory.size(); ++t)
  {
    printf("Trajectory step %d/%d\r", t, mTrueTrajectory.trajectory.size());

    // For each particle
    for (uint i = 0; i < mParticles.size(); ++i)
    {
      Particle& p = mParticles[i];

      // Update robot position according to model params
      float x = p.currentPosition.x;
      float y = p.currentPosition.y;

      x = x + p.params.a - p.params.b*x;
      y = y + p.params.c - p.params.d*y;

      p.currentPosition = Point(x, y);

      float weight = calculateDenormalizedImportanceWeight(mStepToImageMap[t], p.currentPosition);
      p.importanceWeight = weight;
    }

    // Normalize important weights among all particles for this step
    const float eps = float(1e-8);
    float weightsSum = eps;
    for (uint i = 0; i < mParticles.size(); ++i)
    {
      Particle& p = mParticles[i];
      weightsSum += p.importanceWeight;
    }
    for (uint i = 0; i < mParticles.size(); ++i)
    {
      Particle& p = mParticles[i];
      //float finalWeight = p.importanceWeight / weightsSum;
      float finalWeight = std::log(p.importanceWeight / weightsSum);
      //p.importanceWeights.push_back(finalWeight);
      p.importanceWeightsSum += finalWeight;
    }
  }

  // Calculate resulting important weights
  for (uint i = 0; i < mParticles.size(); ++i)
  {
    Particle& p = mParticles[i];
    p.importanceWeight = 0.f;

    //for (uint j = 0; j < p.importanceWeights.size(); ++j)
    //{
    //  //const float th = 0.00001f;
    //  //if (p.importanceWeights[j] < th)
    //  //{
    //  //  p.importanceWeight = 0.f;
    //  //  continue;
    //  //}
    //  p.importanceWeight += p.importanceWeights[j];
    //}
    //p.importanceWeight /= mTrueTrajectory.params.T;

    p.importanceWeight = p.importanceWeightsSum / mTrueTrajectory.params.T;
  }

  mFinished = true;
}

float ParticleFilter::calculateDenormalizedImportanceWeight(const std::vector<Point>& pts, const Point& position)
{
  float dist = 0;

  for (uint i = 0; i < pts.size(); ++i)
  {
    dist += position.euclidianDistance2To(pts[i]);
  }

  // Map weight to (0, 1)
  return 1.f / (1.f + dist);
}


//! Compare particles by their closeness to correct model params in absolute values means
struct ParticleComparatorByCorrectModelClosenessAbs
{
  ParticleComparatorByCorrectModelClosenessAbs()
    : trueParams() { }

  bool operator () (const Particle& p1, const Particle& p2)
  {
    float d1a = p1.params.a - trueParams.a;
    float d1b = p1.params.b - trueParams.b;
    float d1c = p1.params.c - trueParams.c;
    float d1d = p1.params.d - trueParams.d;
    float d1  = d1a*d1a + d1b*d1b + d1c*d1c + d1d*d1d;

    float d2a = p2.params.a - trueParams.a;
    float d2b = p2.params.b - trueParams.b;
    float d2c = p2.params.c - trueParams.c;
    float d2d = p2.params.d - trueParams.d;
    float d2  = d2a*d2a + d2b*d2b + d2c*d2c + d2d*d2d;

    return d1 > d2;
  }

  TrajectoryParams trueParams;
};

//! Compare particles by their closeness to correct model params in relative values means
struct ParticleComparatorByCorrectModelClosenessRel
{
  ParticleComparatorByCorrectModelClosenessRel()
    : trueParams() { }

  bool operator () (const Particle& p1, const Particle& p2)
  {
    float d1a = abs(p1.params.a - trueParams.a);
    float d1b = abs(p1.params.b - trueParams.b);
    float d1c = abs(p1.params.c - trueParams.c);
    float d1d = abs(p1.params.d - trueParams.d);
    float d1  = (d1a/trueParams.a)*(d1a/trueParams.a) + (d1b/trueParams.b)*(d1b/trueParams.b) +
                (d1c/trueParams.c)*(d1c/trueParams.c) + (d1d/trueParams.d)*(d1d/trueParams.d);

    float d2a = abs(p2.params.a - trueParams.a);
    float d2b = abs(p2.params.b - trueParams.b);
    float d2c = abs(p2.params.c - trueParams.c);
    float d2d = abs(p2.params.d - trueParams.d);
    float d2  = (d2a/trueParams.a)*(d2a/trueParams.a) + (d2b/trueParams.b)*(d2b/trueParams.b) +
                (d2c/trueParams.c)*(d2c/trueParams.c) + (d2d/trueParams.d)*(d2d/trueParams.d);

    return d1 < d2;
  }

  TrajectoryParams trueParams;
};


//! Compare particles by their importance weight
struct ParticleComparatorByImportanceWeight
{
  bool operator () (const Particle& p1, const Particle& p2)
  {
    return p1.importanceWeight < p2.importanceWeight;
  }
};

typedef std::vector< std::pair<float, float> > Plot;

void writePlotData(const Plot& plot, const char* labelX, const char* labelY, const char* file)
{
  FILE* f = fopen(file, "w");

  fprintf(f, "ListLinePlot[{");
  for (int i = 0; i < plot.size(); ++i)
  {
    auto p = plot[i];
    fprintf(f, "{%f, %f},", p.first, p.second);
  }
  fprintf(f, "}, Axes -> True, AxesLabel -> {\"%s\", \"%s\"}]", labelX, labelY);

  fclose(f);  
}

void ParticleFilter::printResults(int maxParticles/* = 10*/, bool printClosestToTrue/* = true*/)
{
  if (!mFinished)
  {
    printf("*** Call ParticleFilter::process() first\n");
    return;
  }

  Plot plot_relError_impWeight;

  const int iMax = mParticles.size() - 1;
  const int iMin = iMax - maxParticles + 1;

  {
    std::cout << ">>> Top particles by their importance weight" << std::endl;
    ParticleComparatorByImportanceWeight comp;
    std::sort(mParticles.begin(), mParticles.end(), comp);

    // Plots
    float iMinPlot = mParticles.size() * 0.9;
    float iMinPlotBigStep = mParticles.size() * 0.9;
    float iBigStepPlot = 100;
    for (int i = iMax; i >= iMinPlot; )
    {
      //mParticles[i].printInfo();

      float da = abs(mParticles[i].params.a - mTrueTrajectory.params.a);
      float db = abs(mParticles[i].params.b - mTrueTrajectory.params.b);
      float dc = abs(mParticles[i].params.c - mTrueTrajectory.params.c);
      float dd = abs(mParticles[i].params.d - mTrueTrajectory.params.d);

      float ra = da / mTrueTrajectory.params.a;
      float rb = db / mTrueTrajectory.params.b;
      float rc = dc / mTrueTrajectory.params.c;
      float rd = dd / mTrueTrajectory.params.d;

      float avgRelError = (ra + rb + rc + rd) * 0.25;

      plot_relError_impWeight.push_back(std::make_pair(mParticles[i].importanceWeight, avgRelError));

      //if (i < iMinPlotBigStep)
      //{
        i -= iBigStepPlot;
      //}
      //else
      //{
      //  i -= 5;
      //}
    }

    for (int i = iMax; i >= 0/*iMin*/; --i)
    {
      //mParticles[i].printInfo();

      float da = abs(mParticles[i].params.a - mTrueTrajectory.params.a);
      float db = abs(mParticles[i].params.b - mTrueTrajectory.params.b);
      float dc = abs(mParticles[i].params.c - mTrueTrajectory.params.c);
      float dd = abs(mParticles[i].params.d - mTrueTrajectory.params.d);

      float ra = da / mTrueTrajectory.params.a;
      float rb = db / mTrueTrajectory.params.b;
      float rc = dc / mTrueTrajectory.params.c;
      float rd = dd / mTrueTrajectory.params.d;

      float avgRelError = (ra + rb + rc + rd) * 0.25;

      //mParticles[i].printInfo();
      if (i >= iMin)
      {
        printf("Imp weight: %.3f; Model params: (a=%5.2f, b=%6.3f, c=%5.2f, d=%6.3f) AbsError: (a: %6.3f, b: %6.3f, c: %6.3f, d: %6.3f), \nRelError: (a: %6.3f, b: %6.3f, c: %6.3f, d: %6.3f), AvgRelError: %.5f\n",
          mParticles[i].importanceWeight, mParticles[i].params.a, mParticles[i].params.b, mParticles[i].params.c, mParticles[i].params.d, da, db, dc, dd, ra, rb, rc, rd, avgRelError);
      }
    }
  }

  std::cout << ">>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>>" << std::endl;

  if (printClosestToTrue)
  {
    std::cout << ">>> Top particle by their closeness to true model params absolute" << std::endl;
    ParticleComparatorByCorrectModelClosenessAbs comp1;
    comp1.trueParams = mTrueTrajectory.params;
    std::sort(mParticles.begin(), mParticles.end(), comp1);

    int i = mParticles.size() - 1;
    //for (int i = iMax; i >= iMin; --i)
    {
      float da = abs(mParticles[i].params.a - mTrueTrajectory.params.a);
      float db = abs(mParticles[i].params.b - mTrueTrajectory.params.b);
      float dc = abs(mParticles[i].params.c - mTrueTrajectory.params.c);
      float dd = abs(mParticles[i].params.d - mTrueTrajectory.params.d);

      float ra = da / mTrueTrajectory.params.a;
      float rb = db / mTrueTrajectory.params.b;
      float rc = dc / mTrueTrajectory.params.c;
      float rd = dd / mTrueTrajectory.params.d;

      //mParticles[i].printInfo();
      printf("Imp weight: %.3f; Model params: (a=%5.2f, b=%6.3f, c=%5.2f, d=%6.3f) AbsError: (a: %6.3f, b: %6.3f, c: %6.3f, d: %6.3f), \nRelError: (a: %6.3f, b: %6.3f, c: %6.3f, d: %6.3f)\n",
        mParticles[i].importanceWeight, mParticles[i].params.a, mParticles[i].params.b, mParticles[i].params.c, mParticles[i].params.d, da, db, dc, dd, ra, rb, rc, rd);
    }

#if 0
    std::cout << ">>> Top particle by their closeness to true model params relative" << std::endl;
    ParticleComparatorByCorrectModelClosenessRel comp2;
    comp2.trueParams = mTrueTrajectory.params;
    std::sort(mParticles.begin(), mParticles.end(), comp2);

    //for (int i = iMax; i >= iMin; --i)
    {
      float da = abs(mParticles[i].params.a - mTrueTrajectory.params.a);
      float db = abs(mParticles[i].params.b - mTrueTrajectory.params.b);
      float dc = abs(mParticles[i].params.c - mTrueTrajectory.params.c);
      float dd = abs(mParticles[i].params.d - mTrueTrajectory.params.d);

      float ra = da / mTrueTrajectory.params.a;
      float rb = db / mTrueTrajectory.params.b;
      float rc = dc / mTrueTrajectory.params.c;
      float rd = dd / mTrueTrajectory.params.d;

      //mParticles[i].printInfo();
      printf("Imp weight: %.3f; Model params: (a=%5.2f, b=%6.3f, c=%5.2f, d=%6.3f) AbsError: (a: %6.3f, b: %6.3f, c: %6.3f, d: %6.3f), \nRelError: (a: %6.3f, b: %6.3f, c: %6.3f, d: %6.3f)\n",
        mParticles[i].importanceWeight, mParticles[i].params.a, mParticles[i].params.b, mParticles[i].params.c, mParticles[i].params.d, da, db, dc, dd, ra, rb, rc, rd);
    }
#endif
  }

  writePlotData(plot_relError_impWeight, "Importance weight", "Relative error", "plot_relError_impWeight.txt");

  //printf("<No results>\n");  
}

void ParticleFilter::printErrorForBestParticle()
{
  if (!mFinished)
  {
    printf("*** Call ParticleFilter::process() first\n");
    return;
  }

  const int iMax = mParticles.size() - 1;
  const int iMin = iMax;

  {
    ParticleComparatorByImportanceWeight comp;
    std::sort(mParticles.begin(), mParticles.end(), comp);

    for (int i = iMax; i >= iMin; --i)
    {
      float da = abs(mParticles[i].params.a - mTrueTrajectory.params.a);
      float db = abs(mParticles[i].params.b - mTrueTrajectory.params.b);
      float dc = abs(mParticles[i].params.c - mTrueTrajectory.params.c);
      float dd = abs(mParticles[i].params.d - mTrueTrajectory.params.d);
      printf("T: %3d, N: %8d, Error: (a: %6.3f, b: %6.3f, c: %6.3f, d: %6.3f)\n",
             mTrueTrajectory.params.T, mParticles.size(), da, db, dc, dd);
    }
  }
}

