import itertools
from bidict import bidict
from functools import *
from pprint import pprint
from lib.distributions import *
from lib.utils import *

_DEBUG = False

def code_to_integer(code):
    result = 0
    for i in range(0, len(code)):
        result += (code[i] << (len(code) - i - 1))
    return result

def get_possible_letters_for_codes_using_confidence_interval(bits, code_length, frequencies_theoretical):
    # Firstly, count frequency for each possible code
    code_frequencies = {}
    for code in range(0, 2**code_length):
        code_frequencies[code] = 0

    # Floor
    text_length = int(len(bits) / code_length)
    # Process bit stream
    for i in range(0, text_length):
        pos = i * code_length
        code = []
        for j in range(pos, pos + code_length):
            code.append(bits[j])
        if _DEBUG:
            print(code, code_to_integer(code))
        code = code_to_integer(code)

        # Check if code is already presents in map
        try:
            freq = code_frequencies[code]
        except KeyError:
            code_frequencies[code] = 0

        code_frequencies[code] += 1

    # Normalize
    for k, v in code_frequencies.items():
        code_frequencies[k] = float(v) / text_length

    # Confidence interval
    # FIXME
    possible_letters = {}
    betta = 0.05
    p = (1 + betta) / 2 # 0.525
    theta_betta = 3.5
    L = text_length
    for char, ft in frequencies_theoretical.items():
        interval_width = theta_betta * math.sqrt((ft * (1 - ft)) / L)
        lower_bound = ft - interval_width
        upper_bound = ft + interval_width

        for code, fo in code_frequencies.items():
            if lower_bound <= fo <= upper_bound:
                # Check if code is already presents in map
                try:
                    letters = possible_letters[code]
                except KeyError:
                    possible_letters[code] = []
                possible_letters[code].append(char)

    #for k, v in sorted(code_frequencies.items()):
    #    print("code {0}:{1}".format(k, v))

    return possible_letters

"""
Params:
        code_length - code length
        n - number of bijection in [0,(2**code_length)! - 1] range
"""
def bijection_ij(code_length, n):
    # Omega
    codes = []
    for idx in range(0, 2**code_length):
        codes.append(idx)

    #print(codes)

    bijection = bidict({})

    # Slow even for i = 4 (16! permutations)
    # perms = list(itertools.permutations(codes))
    # perm = perms[j]
    iter = itertools.permutations(codes)
    for i in range(0, n + 1):
        perm = next(iter)

    print(perm)

    for i in range(0, 2**code_length):
        #print(idx, i)
        bijection[chr(ord('a') + i)] = perm[i]

    return bijection

def make_bijection(possible_letters, indices):
    bijection = bidict({})

    for code, idx in indices.items():
        #print(code, idx)
        bijection[possible_letters[code][idx]] = code

    return bijection

def gen_possible_bijections(possible_letters):
    bijections = []

    # Convert to list of tuples
    tuples = list(possible_letters.items())
    N = len(tuples)

    # Support indices
    indices = {}
    for i in range(0, N):
        code, letters = tuples[i]
        indices[code] = 0

    # Combinatorial algorithm
    bijections.append(make_bijection(possible_letters, indices))
    idx_cur = 0
    idx_last = N - 1
    went_up = False
    while True:
        # Check if code's current letter index already reached max value
        code, letters = tuples[idx_cur]
        reached_end = (indices[code] >= len(letters) - 1)
        if reached_end:
            # Reset index
            indices[code] = 0
            # Go up
            if idx_cur > 0:
                idx_cur -= 1
                went_up = True
                continue
            else:
                break
        else:
            if idx_cur < idx_last:
                if went_up:
                    indices[code] += 1
                    # Make new bijection
                    bijections.append(make_bijection(possible_letters, indices))
                    went_up = False
                # Go down
                idx_cur += 1
                continue
            else:
                indices[code] += 1
                # Make new bijection
                bijections.append(make_bijection(possible_letters, indices))

    return bijections


def translate_bits_to_text(bits, code_length, bijection):
    text = []

    # Floor
    text_length = int(len(bits) / code_length)
    # Process bit stream
    for i in range(0, text_length):
        pos = i * code_length
        code = []
        for j in range(pos, pos + code_length):
            code.append(bits[j])
        if _DEBUG:
            print(code, code_to_integer(code))
        code = code_to_integer(code)
        char = bijection[:code]
        text.append(char)

    return text

def count_observed_frequencies(text, characters):
    text_length = len(text)
    frequencies = {}
    for k in characters.keys():
        frequencies[k] = 0
    for c in text:
        # Check if this character is allowed
        try:
            value = characters[c]
        except KeyError:
            continue
        frequencies[c] += 1

    for k, v in frequencies.items():
        frequencies[k] = float(v) / text_length

    return frequencies

def chi_squared_statistics(frequencies_theoretical, frequencies_observed):
    chi_squared = 0
    for char, ft in frequencies_theoretical.items():
        # Normalize observed frequencies
        norm = 1 - ft
        fo = frequencies_observed[char] * norm
        chi_squared += (fo - ft) * (fo - ft) / ft

    df = len(frequencies_theoretical.keys()) - 1

    return chi_squared, df

def check_hypothesis(statistic, threshold):
    return statistic <= threshold

def main():
    # uniform = unifom_distributed_numbers(10, 2, 100)
    # print([i for i in uniform])
    #
    # normal = normal_distributed_numbers(10, 0, 1)
    # print([i for i in normal])
    #
    # chi_square = chi_squared_distributed_numbers(10, 10)
    # print([i for i in chi_square])

    # Read theoretical frequencies
    frequencies_theoretical = read_frequencies('freq.txt')
    # print(frequencies_theoretical)

    # Read input stream of bits
    bits = []
    for b in read_bits('bitstream2.bin'):
        bits.append(b)
    if _DEBUG:
        print(bits)

    # Choose length of a code
    code_length = 5
    # Choose a bijection
    #bij = bijection_ij(code_length, 12345)
    bij = bijection_ij(code_length, 1)
    # pprint(bij)
    #for k, v in sorted(bij.items()):
    #    print("{0}:{1}".format(k, bin(v)))

    #possible_letters_for_codes = get_possible_letters_for_codes_using_confidence_interval(bits, code_length, frequencies_theoretical)
    #possible_letters_for_codes = {0: ['a', 'b'], 1: ['c', 'd'], 2: ['e', 'f']}
    possible_letters_for_codes = {0: ['a', 'b'], 1: ['a', 'd']}
    for k, v in sorted(possible_letters_for_codes.items()):
        print("code {0}:{1}".format(k, v))

    bijections = gen_possible_bijections(possible_letters_for_codes)

    print("Possible bijections:")
    for bijection in bijections:
        for k, v in sorted(bijection.items()):
            #print("{0}:{1}".format(k, bin(v)))
            print("{0}:{1}".format(k, format(v, '#04b')))
        print("--------------")

    return

    text = translate_bits_to_text(bits, code_length, bij)
    #print(text)
    print(reduce(lambda a, b: ''.join([a, b]), text))

    frequencies_observed = count_observed_frequencies(text, frequencies_theoretical)
    print(frequencies_observed)

    threshold_25_995 = 10.52
    threshold_25_950 = 14.611

    st, df = chi_squared_statistics(frequencies_theoretical, frequencies_observed)
    print(st, df)

    accept = check_hypothesis(st, threshold_25_950)
    if accept is True:
        print("Hypothesis has been accepted")
    else:
        print("Hypothesis has been rejected")

if __name__ == "__main__":
    main()
