import random
import math

"""
Returns generator for uniform distributed random numbers
Params:
        N - number of values in generator
        min - min value
        max - max value
"""
def unifom_distributed_numbers(N, min, max):
    for i in range(0, N):
        yield min + random.random() * (max - min)

"""
Returns generator for normal distributed random numbers
Params:
        N - number of values in generator
        mean - mean value
        sigma2 - variance of normal distribution

Source: http://en.wikipedia.org/wiki/Box_Muller_transform
"""
def normal_distributed_numbers(N, mean=0, sigma2=1):
    # u, v are independent random variables that are uniformly distributed in the interval (0, 1]
    for i in range(0, N):
        u = 0
        while True:
            u = [i for i in unifom_distributed_numbers(1, 0, 1)][0]
            if u == 0:
                pass
            else:
                break

        v = [i for i in unifom_distributed_numbers(1, 0, 1)][0]

        # FIXME: rounding bias here!
        # z0 and z1 are independent random variables with a standard normal distribution
        z0 = sigma2 * math.sqrt (-2 * math.log (u)) * math.cos (2 * math.pi * v);
        z1 = sigma2 * math.sqrt (-2 * math.log (u)) * math.sin (2 * math.pi * v);

        yield mean + z0

"""
Returns generator for chi-squared distributed random numbers
Params:
        r - number of degrees of freedom

Source: http://mathworld.wolfram.com/Chi-SquaredDistribution.html
"""
def chi_squared_distributed_numbers(N, r):
    for i in range(0, N):
        normald = [i for i in unifom_distributed_numbers(r, 0, 1)]
        chi_squared = 0
        # accumulate squares of normal distributed values
        for n in normald:
            chi_squared += n * n

        yield chi_squared
