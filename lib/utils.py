

"""
Read frequencies of english alphabet characters from file
Input file format is

a 8.2%
b 1.0%
...

Params:
        path - Input file path
"""
def read_frequencies(path):
    f = open(path, 'r')
    frequencies = {}
    for line in f.read().splitlines():
        tokens = line.split()
        if len(tokens) == 2:
            c, f = tokens
            if not c.isalpha():
                continue
            f = float(f.replace('%', '')) / 100
            frequencies[c] = f

            # if _DEBUG:
            #     print(c, f)

    return frequencies

def read_bits(path):
    f = open(path, 'r')
    bytes = (ord(b) for b in f.read())
    #bytes = (b for b in f.read())
    for b in bytes:
        for i in range(8):
            yield (b >> i) & 1



